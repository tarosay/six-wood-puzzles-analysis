﻿namespace 組み木解法
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSet = new System.Windows.Forms.Button();
            this.tbxMenNum = new System.Windows.Forms.TextBox();
            this.lblMenNumTitle = new System.Windows.Forms.Label();
            this.tbx00 = new System.Windows.Forms.TextBox();
            this.tbx01 = new System.Windows.Forms.TextBox();
            this.tbx10 = new System.Windows.Forms.TextBox();
            this.tbx11 = new System.Windows.Forms.TextBox();
            this.tbx20 = new System.Windows.Forms.TextBox();
            this.tbx21 = new System.Windows.Forms.TextBox();
            this.tbx30 = new System.Windows.Forms.TextBox();
            this.tbx31 = new System.Windows.Forms.TextBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.lblPartsTitle = new System.Windows.Forms.Label();
            this.tbxParts = new System.Windows.Forms.TextBox();
            this.gbxParts = new System.Windows.Forms.GroupBox();
            this.btnMenShift = new System.Windows.Forms.Button();
            this.btnMenDown = new System.Windows.Forms.Button();
            this.btnMenUp = new System.Windows.Forms.Button();
            this.btnCalc = new System.Windows.Forms.Button();
            this.btnPartsDown = new System.Windows.Forms.Button();
            this.btnPartsUp = new System.Windows.Forms.Button();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.btnRot = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnAlAnal = new System.Windows.Forms.Button();
            this.lblCountTitle = new System.Windows.Forms.Label();
            this.tbxCount = new System.Windows.Forms.TextBox();
            this.gbxParts.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(6, 109);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(99, 51);
            this.btnSet.TabIndex = 5;
            this.btnSet.Text = "面セット";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // tbxMenNum
            // 
            this.tbxMenNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbxMenNum.Location = new System.Drawing.Point(62, 15);
            this.tbxMenNum.Name = "tbxMenNum";
            this.tbxMenNum.Size = new System.Drawing.Size(41, 23);
            this.tbxMenNum.TabIndex = 1;
            this.tbxMenNum.Text = "0";
            // 
            // lblMenNumTitle
            // 
            this.lblMenNumTitle.AutoSize = true;
            this.lblMenNumTitle.Location = new System.Drawing.Point(15, 22);
            this.lblMenNumTitle.Name = "lblMenNumTitle";
            this.lblMenNumTitle.Size = new System.Drawing.Size(41, 12);
            this.lblMenNumTitle.TabIndex = 0;
            this.lblMenNumTitle.Text = "面番号";
            // 
            // tbx00
            // 
            this.tbx00.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx00.Location = new System.Drawing.Point(139, 15);
            this.tbx00.Name = "tbx00";
            this.tbx00.Size = new System.Drawing.Size(40, 23);
            this.tbx00.TabIndex = 6;
            this.tbx00.Text = "0";
            // 
            // tbx01
            // 
            this.tbx01.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx01.Location = new System.Drawing.Point(197, 15);
            this.tbx01.Name = "tbx01";
            this.tbx01.Size = new System.Drawing.Size(40, 23);
            this.tbx01.TabIndex = 7;
            this.tbx01.Text = "0";
            // 
            // tbx10
            // 
            this.tbx10.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx10.Location = new System.Drawing.Point(139, 39);
            this.tbx10.Name = "tbx10";
            this.tbx10.Size = new System.Drawing.Size(40, 23);
            this.tbx10.TabIndex = 8;
            this.tbx10.Text = "0";
            // 
            // tbx11
            // 
            this.tbx11.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx11.Location = new System.Drawing.Point(197, 39);
            this.tbx11.Name = "tbx11";
            this.tbx11.Size = new System.Drawing.Size(40, 23);
            this.tbx11.TabIndex = 9;
            this.tbx11.Text = "0";
            // 
            // tbx20
            // 
            this.tbx20.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx20.Location = new System.Drawing.Point(139, 64);
            this.tbx20.Name = "tbx20";
            this.tbx20.Size = new System.Drawing.Size(40, 23);
            this.tbx20.TabIndex = 10;
            this.tbx20.Text = "0";
            // 
            // tbx21
            // 
            this.tbx21.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx21.Location = new System.Drawing.Point(197, 64);
            this.tbx21.Name = "tbx21";
            this.tbx21.Size = new System.Drawing.Size(40, 23);
            this.tbx21.TabIndex = 11;
            this.tbx21.Text = "0";
            // 
            // tbx30
            // 
            this.tbx30.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx30.Location = new System.Drawing.Point(139, 89);
            this.tbx30.Name = "tbx30";
            this.tbx30.Size = new System.Drawing.Size(40, 23);
            this.tbx30.TabIndex = 12;
            this.tbx30.Text = "0";
            // 
            // tbx31
            // 
            this.tbx31.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbx31.Location = new System.Drawing.Point(197, 89);
            this.tbx31.Name = "tbx31";
            this.tbx31.Size = new System.Drawing.Size(40, 23);
            this.tbx31.TabIndex = 13;
            this.tbx31.Text = "0";
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(135, 124);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(102, 36);
            this.btnCheck.TabIndex = 14;
            this.btnCheck.Text = "整合性のチェック";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // lblPartsTitle
            // 
            this.lblPartsTitle.AutoSize = true;
            this.lblPartsTitle.Location = new System.Drawing.Point(12, 15);
            this.lblPartsTitle.Name = "lblPartsTitle";
            this.lblPartsTitle.Size = new System.Drawing.Size(58, 12);
            this.lblPartsTitle.TabIndex = 12;
            this.lblPartsTitle.Text = "パーツ番号";
            // 
            // tbxParts
            // 
            this.tbxParts.Location = new System.Drawing.Point(76, 12);
            this.tbxParts.Name = "tbxParts";
            this.tbxParts.Size = new System.Drawing.Size(28, 19);
            this.tbxParts.TabIndex = 0;
            this.tbxParts.Text = "0";
            // 
            // gbxParts
            // 
            this.gbxParts.Controls.Add(this.btnMenShift);
            this.gbxParts.Controls.Add(this.btnMenDown);
            this.gbxParts.Controls.Add(this.btnMenUp);
            this.gbxParts.Controls.Add(this.lblMenNumTitle);
            this.gbxParts.Controls.Add(this.tbxMenNum);
            this.gbxParts.Controls.Add(this.tbx00);
            this.gbxParts.Controls.Add(this.btnCheck);
            this.gbxParts.Controls.Add(this.tbx01);
            this.gbxParts.Controls.Add(this.btnSet);
            this.gbxParts.Controls.Add(this.tbx31);
            this.gbxParts.Controls.Add(this.tbx10);
            this.gbxParts.Controls.Add(this.tbx30);
            this.gbxParts.Controls.Add(this.tbx11);
            this.gbxParts.Controls.Add(this.tbx21);
            this.gbxParts.Controls.Add(this.tbx20);
            this.gbxParts.Location = new System.Drawing.Point(14, 110);
            this.gbxParts.Name = "gbxParts";
            this.gbxParts.Size = new System.Drawing.Size(259, 166);
            this.gbxParts.TabIndex = 14;
            this.gbxParts.TabStop = false;
            this.gbxParts.Text = "パーツデータ";
            // 
            // btnMenShift
            // 
            this.btnMenShift.Location = new System.Drawing.Point(6, 75);
            this.btnMenShift.Name = "btnMenShift";
            this.btnMenShift.Size = new System.Drawing.Size(99, 28);
            this.btnMenShift.TabIndex = 4;
            this.btnMenShift.Text = "面シフト";
            this.btnMenShift.UseVisualStyleBackColor = true;
            this.btnMenShift.Click += new System.EventHandler(this.btnMenShift_Click);
            // 
            // btnMenDown
            // 
            this.btnMenDown.Location = new System.Drawing.Point(6, 46);
            this.btnMenDown.Name = "btnMenDown";
            this.btnMenDown.Size = new System.Drawing.Size(41, 23);
            this.btnMenDown.TabIndex = 2;
            this.btnMenDown.Text = "↓";
            this.btnMenDown.UseVisualStyleBackColor = true;
            this.btnMenDown.Click += new System.EventHandler(this.btnMenDown_Click);
            // 
            // btnMenUp
            // 
            this.btnMenUp.Location = new System.Drawing.Point(64, 46);
            this.btnMenUp.Name = "btnMenUp";
            this.btnMenUp.Size = new System.Drawing.Size(41, 23);
            this.btnMenUp.TabIndex = 3;
            this.btnMenUp.Text = "↑";
            this.btnMenUp.UseVisualStyleBackColor = true;
            this.btnMenUp.Click += new System.EventHandler(this.btnMenUp_Click);
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(116, 12);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(102, 28);
            this.btnCalc.TabIndex = 4;
            this.btnCalc.Text = "計算開始";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // btnPartsDown
            // 
            this.btnPartsDown.Location = new System.Drawing.Point(22, 37);
            this.btnPartsDown.Name = "btnPartsDown";
            this.btnPartsDown.Size = new System.Drawing.Size(39, 23);
            this.btnPartsDown.TabIndex = 1;
            this.btnPartsDown.Text = "↓";
            this.btnPartsDown.UseVisualStyleBackColor = true;
            this.btnPartsDown.Click += new System.EventHandler(this.btnPartsDown_Click);
            // 
            // btnPartsUp
            // 
            this.btnPartsUp.Location = new System.Drawing.Point(65, 37);
            this.btnPartsUp.Name = "btnPartsUp";
            this.btnPartsUp.Size = new System.Drawing.Size(39, 23);
            this.btnPartsUp.TabIndex = 2;
            this.btnPartsUp.Text = "↑";
            this.btnPartsUp.UseVisualStyleBackColor = true;
            this.btnPartsUp.Click += new System.EventHandler(this.btnPartsUp_Click);
            // 
            // tbxMessage
            // 
            this.tbxMessage.Location = new System.Drawing.Point(14, 282);
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.Size = new System.Drawing.Size(259, 19);
            this.tbxMessage.TabIndex = 18;
            this.tbxMessage.TabStop = false;
            // 
            // btnRot
            // 
            this.btnRot.Location = new System.Drawing.Point(22, 76);
            this.btnRot.Name = "btnRot";
            this.btnRot.Size = new System.Drawing.Size(82, 28);
            this.btnRot.TabIndex = 3;
            this.btnRot.Text = "パーツ回転";
            this.btnRot.UseVisualStyleBackColor = true;
            this.btnRot.Click += new System.EventHandler(this.btnRot_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(196, 76);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 28);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "パーツ保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(116, 76);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(77, 28);
            this.btnRead.TabIndex = 5;
            this.btnRead.Text = "パーツ読込";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnAlAnal
            // 
            this.btnAlAnal.Location = new System.Drawing.Point(116, 42);
            this.btnAlAnal.Name = "btnAlAnal";
            this.btnAlAnal.Size = new System.Drawing.Size(102, 28);
            this.btnAlAnal.TabIndex = 19;
            this.btnAlAnal.Text = "全パターン解析";
            this.btnAlAnal.UseVisualStyleBackColor = true;
            this.btnAlAnal.Click += new System.EventHandler(this.btnAlAnal_Click);
            // 
            // lblCountTitle
            // 
            this.lblCountTitle.AutoSize = true;
            this.lblCountTitle.Location = new System.Drawing.Point(226, 20);
            this.lblCountTitle.Name = "lblCountTitle";
            this.lblCountTitle.Size = new System.Drawing.Size(53, 12);
            this.lblCountTitle.TabIndex = 20;
            this.lblCountTitle.Text = "解析回数";
            // 
            // tbxCount
            // 
            this.tbxCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbxCount.Location = new System.Drawing.Point(224, 35);
            this.tbxCount.Name = "tbxCount";
            this.tbxCount.Size = new System.Drawing.Size(55, 23);
            this.tbxCount.TabIndex = 21;
            this.tbxCount.TabStop = false;
            this.tbxCount.Text = "0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 313);
            this.Controls.Add(this.tbxCount);
            this.Controls.Add(this.lblCountTitle);
            this.Controls.Add(this.btnAlAnal);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnRot);
            this.Controls.Add(this.tbxMessage);
            this.Controls.Add(this.btnPartsUp);
            this.Controls.Add(this.btnPartsDown);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.gbxParts);
            this.Controls.Add(this.tbxParts);
            this.Controls.Add(this.lblPartsTitle);
            this.Name = "MainForm";
            this.Text = "組み木解法";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.gbxParts.ResumeLayout(false);
            this.gbxParts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox tbxMenNum;
        private System.Windows.Forms.Label lblMenNumTitle;
        private System.Windows.Forms.TextBox tbx00;
        private System.Windows.Forms.TextBox tbx01;
        private System.Windows.Forms.TextBox tbx10;
        private System.Windows.Forms.TextBox tbx11;
        private System.Windows.Forms.TextBox tbx20;
        private System.Windows.Forms.TextBox tbx21;
        private System.Windows.Forms.TextBox tbx30;
        private System.Windows.Forms.TextBox tbx31;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label lblPartsTitle;
        private System.Windows.Forms.TextBox tbxParts;
        private System.Windows.Forms.GroupBox gbxParts;
        private System.Windows.Forms.Button btnMenDown;
        private System.Windows.Forms.Button btnMenUp;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.Button btnPartsDown;
        private System.Windows.Forms.Button btnPartsUp;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.Button btnRot;
        private System.Windows.Forms.Button btnMenShift;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnAlAnal;
        private System.Windows.Forms.Label lblCountTitle;
        private System.Windows.Forms.TextBox tbxCount;
    }
}

