﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 組み木解法
{
    public class KumiTree
    {
        /// <summary>
        ///          X0 X1            X0 X1            X0 X1            X0 X1
        /// Z0 = Y0|~○ ○~| Z1 = Y0|~○ ○~| Z2 = Y0|~○ ○~| Z3 = Y0|~○ ○~|
        ///      Y1|_○ ○_|      Y1|_○ ○_|      Y1|_○ ○_|      Y1|_○ ○_|  とする。
        /// </summary>
        public int[, ,] Parts = new int[4, 2, 2];       //Z, Y, X

        /// <summary>
        ///        X0=Y0 Y1           Y1=X0 X1           X1=Y1 Y0           Y0=X1 X0
        /// M0 = Z0 |~○ ○~|  M1 = Z0 |~○ ○~|  M2 = Z0 |~○ ○~|  M3 = Z0 |~○ ○~| 
        ///      Z1 | ○ ○ |       Z1 | ○ ○ |       Z1 | ○ ○ |       Z1 | ○ ○ | 
        ///      Z2 | ○ ○ |       Z2 | ○ ○ |       Z2 | ○ ○ |       Z2 | ○ ○ | 
        ///      Z3 |_○ ○_|       Z3 |_○ ○_|       Z3 |_○ ○_|       Z3 |_○ ○_|  とする。
        /// </summary>
        public int[, ,] Faces = new int[4, 4, 2];

        public KumiTree()
        {

        }

        /// <summary>
        /// フェイスデータをセットします
        /// 面menの配列を用意してセットします
        /// </summary>
        /// <param name="dat"></param>
        public bool SetFace(int[,] dat, int men)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Faces[men, i, j] = dat[i, j];
                }
            }
            return true;
        }

        /// <summary>
        /// フェイスデータをセットします
        /// 面0の右縦列、左縦列、面1の右縦列、左縦列、面2の右縦列、左縦列、面3の右縦列、左縦列と配列を用意してセットします
        /// </summary>
        /// <param name="dat"></param>
        public bool SetFace(int[,] dat)
        {
            Faces[0, 0, 0] = dat[0, 0];
            Faces[0, 1, 0] = dat[0, 1];
            Faces[0, 2, 0] = dat[0, 2];
            Faces[0, 3, 0] = dat[0, 3];

            Faces[0, 0, 1] = dat[1, 0];
            Faces[0, 1, 1] = dat[1, 1];
            Faces[0, 2, 1] = dat[1, 2];
            Faces[0, 3, 1] = dat[1, 3];

            Faces[1, 0, 0] = dat[2, 0];
            Faces[1, 1, 0] = dat[2, 1];
            Faces[1, 2, 0] = dat[2, 2];
            Faces[1, 3, 0] = dat[2, 3];

            Faces[1, 0, 1] = dat[3, 0];
            Faces[1, 1, 1] = dat[3, 1];
            Faces[1, 2, 1] = dat[3, 2];
            Faces[1, 3, 1] = dat[3, 3];

            Faces[2, 0, 0] = dat[4, 0];
            Faces[2, 1, 0] = dat[4, 1];
            Faces[2, 2, 0] = dat[4, 2];
            Faces[2, 3, 0] = dat[4, 3];

            Faces[2, 0, 1] = dat[5, 0];
            Faces[2, 1, 1] = dat[5, 1];
            Faces[2, 2, 1] = dat[5, 2];
            Faces[2, 3, 1] = dat[5, 3];

            Faces[3, 0, 0] = dat[6, 0];
            Faces[3, 1, 0] = dat[6, 1];
            Faces[3, 2, 0] = dat[6, 2];
            Faces[3, 3, 0] = dat[6, 3];

            Faces[3, 0, 1] = dat[7, 0];
            Faces[3, 1, 1] = dat[7, 1];
            Faces[3, 2, 1] = dat[7, 2];
            Faces[3, 3, 1] = dat[7, 3];

            //整合性チェック
            if (CheckFace() == false)
            {
                return false;
            }
            SetParts();

            return true;
        }


        /// <summary>
        /// 面データの整合性をチェックします
        /// </summary>
        /// <returns></returns>
        public bool CheckFace()
        {
            for (int i = 0; i < 4; i++)
            {
                if (Faces[0, i, 0] != Faces[3, i, 1])
                {
                    return false;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (Faces[0, i, 1] != Faces[1, i, 0])
                {
                    return false;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (Faces[1, i, 1] != Faces[2, i, 0])
                {
                    return false;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (Faces[2, i, 1] != Faces[3, i, 0])
                {
                    return false;
                }
            }

            SetParts();

            return true;
        }

        /// <summary>
        /// パーツ配列にデータをセットします
        /// </summary>
        public void SetParts()
        {
            Parts[0, 0, 0] = Faces[0, 0, 0];
            Parts[0, 0, 1] = Faces[3, 0, 0];
            Parts[0, 1, 0] = Faces[1, 0, 0];
            Parts[0, 1, 1] = Faces[2, 0, 0];

            Parts[1, 0, 0] = Faces[0, 1, 0];
            Parts[1, 0, 1] = Faces[3, 1, 0];
            Parts[1, 1, 0] = Faces[1, 1, 0];
            Parts[1, 1, 1] = Faces[2, 1, 0];

            Parts[2, 0, 0] = Faces[0, 2, 0];
            Parts[2, 0, 1] = Faces[3, 2, 0];
            Parts[2, 1, 0] = Faces[1, 2, 0];
            Parts[2, 1, 1] = Faces[2, 2, 0];

            Parts[3, 0, 0] = Faces[0, 3, 0];
            Parts[3, 0, 1] = Faces[3, 3, 0];
            Parts[3, 1, 0] = Faces[1, 3, 0];
            Parts[3, 1, 1] = Faces[2, 3, 0];
        }


        /// <summary>
        /// 面のデータを返します
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public int[,] GetFace(int m)
        {
            int[,] face = new int[4, 2];

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    face[i, j] = Faces[m, i, j];
                }
            }
            return face;
        }

        /// <summary>
        /// 面のデータを返します
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public int[,] GetFace(int m, int ang)
        {
            int[,] face = new int[4, 2];

            if (ang == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        face[i, j] = Faces[m, i, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        face[i, j] = Faces[m, 3 - i, 2 - j];
                    }
                }
            }
            return face;
        }

        /// <summary>
        /// 面データを反転します
        /// </summary>
        /// <param name="f"></param>
        /// <param name="xis"></param>
        /// <returns></returns>
        public int[,] Rev(int[,] f, int xis)
        {
            int[,] face = new int[4, 2];

            if (xis == 0)
            {
                //左右の反転
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        face[i, j] = f[i, 1 - j];
                    }
                }
            }
            else
            {
                //上下の反転
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        face[i, j] = f[3 - i, j];
                    }
                }
            }
            return face;
        }

        /// <summary>
        /// パーツデータを180度回転します
        /// </summary>
        /// <param name="f"></param>
        /// <param name="xis"></param>
        /// <returns></returns>
        public KumiTree Rotate()
        {
            KumiTree kt = new KumiTree();
            int[,] dat = new int[8, 4];

            dat[0, 0] = Faces[0, 3, 1];
            dat[0, 1] = Faces[0, 2, 1];
            dat[0, 2] = Faces[0, 1, 1];
            dat[0, 3] = Faces[0, 0, 1];

            dat[1, 0] = Faces[0, 3, 0];
            dat[1, 1] = Faces[0, 2, 0];
            dat[1, 2] = Faces[0, 1, 0];
            dat[1, 3] = Faces[0, 0, 0];

            dat[2, 0] = Faces[3, 3, 1];
            dat[2, 1] = Faces[3, 2, 1];
            dat[2, 2] = Faces[3, 1, 1];
            dat[2, 3] = Faces[3, 0, 1];

            dat[3, 0] = Faces[3, 3, 0];
            dat[3, 1] = Faces[3, 2, 0];
            dat[3, 2] = Faces[3, 1, 0];
            dat[3, 3] = Faces[3, 0, 0];

            dat[4, 0] = Faces[2, 3, 1];
            dat[4, 1] = Faces[2, 2, 1];
            dat[4, 2] = Faces[2, 1, 1];
            dat[4, 3] = Faces[2, 0, 1];

            dat[5, 0] = Faces[2, 3, 0];
            dat[5, 1] = Faces[2, 2, 0];
            dat[5, 2] = Faces[2, 1, 0];
            dat[5, 3] = Faces[2, 0, 0];

            dat[6, 0] = Faces[1, 3, 1];
            dat[6, 1] = Faces[1, 2, 1];
            dat[6, 2] = Faces[1, 1, 1];
            dat[6, 3] = Faces[1, 0, 1];

            dat[7, 0] = Faces[1, 3, 0];
            dat[7, 1] = Faces[1, 2, 0];
            dat[7, 2] = Faces[1, 1, 0];
            dat[7, 3] = Faces[1, 0, 0];

            if (kt.SetFace(dat) == false)
            {
                return null;
            }
            return kt;
        }

        /// <summary>
        /// 指定した面番号を0番の面にします
        /// </summary>
        /// <param name="men"></param>
        public KumiTree SiftFaceNum(int men)
        {
            KumiTree kt = new KumiTree();
            int[,] dat = new int[8, 4];

            dat[0, 0] = Faces[men, 0, 0];
            dat[0, 1] = Faces[men, 1, 0];
            dat[0, 2] = Faces[men, 2, 0];
            dat[0, 3] = Faces[men, 3, 0];
            dat[1, 0] = Faces[men, 0, 1];
            dat[1, 1] = Faces[men, 1, 1];
            dat[1, 2] = Faces[men, 2, 1];
            dat[1, 3] = Faces[men, 3, 1];
            men++;
            if (men >= 4) { men = 0; }

            dat[2, 0] = Faces[men, 0, 0];
            dat[2, 1] = Faces[men, 1, 0];
            dat[2, 2] = Faces[men, 2, 0];
            dat[2, 3] = Faces[men, 3, 0];
            dat[3, 0] = Faces[men, 0, 1];
            dat[3, 1] = Faces[men, 1, 1];
            dat[3, 2] = Faces[men, 2, 1];
            dat[3, 3] = Faces[men, 3, 1];
            men++;
            if (men >= 4) { men = 0; }

            dat[4, 0] = Faces[men, 0, 0];
            dat[4, 1] = Faces[men, 1, 0];
            dat[4, 2] = Faces[men, 2, 0];
            dat[4, 3] = Faces[men, 3, 0];
            dat[5, 0] = Faces[men, 0, 1];
            dat[5, 1] = Faces[men, 1, 1];
            dat[5, 2] = Faces[men, 2, 1];
            dat[5, 3] = Faces[men, 3, 1];
            men++;
            if (men >= 4) { men = 0; }

            dat[6, 0] = Faces[men, 0, 0];
            dat[6, 1] = Faces[men, 1, 0];
            dat[6, 2] = Faces[men, 2, 0];
            dat[6, 3] = Faces[men, 3, 0];
            dat[7, 0] = Faces[men, 0, 1];
            dat[7, 1] = Faces[men, 1, 1];
            dat[7, 2] = Faces[men, 2, 1];
            dat[7, 3] = Faces[men, 3, 1];

            if (kt.SetFace(dat) == false)
            {
                return null;
            }
            return kt;
        }


    }
}
