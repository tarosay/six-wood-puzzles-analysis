﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 組み木解法
{
    class KumiType
    {
        public static void SetType1(KumiTree[] kt)
        {
            {
                int[,] f0 = { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, };
                kt[0].SetFace(f0, 0);

                int[,] f1 = { { 0, 1 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, };
                kt[0].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 0, 1 }, { 0, 1 }, { 1, 1 }, };
                kt[0].SetFace(f2, 2);

                int[,] f3 = { { 1, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, };
                kt[0].SetFace(f3, 3);
            }

            {
                int[,] f0 = { { 1, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, };
                kt[1].SetFace(f0, 0);

                int[,] f1 = { { 1, 1 }, { 1, 1 }, { 0, 1 }, { 0, 1 }, };
                kt[1].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 1, 0 }, { 1, 0 }, { 1, 1 }, };
                kt[1].SetFace(f2, 2);

                int[,] f3 = { { 1, 1 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, };
                kt[1].SetFace(f3, 3);
            }

            {
                int[,] f0 = { { 0, 0 }, { 0, 0 }, { 0, 1 }, { 1, 1 }, };
                kt[2].SetFace(f0, 0);

                int[,] f1 = { { 0, 1 }, { 0, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[2].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 1, 0 }, { 1, 0 }, { 1, 1 }, };
                kt[2].SetFace(f2, 2);

                int[,] f3 = { { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 1 }, };
                kt[2].SetFace(f3, 3);
            }

            {
                int[,] f0 = { { 0,0 }, { 0, 0 }, { 1, 1 }, { 0, 0 }, };
                kt[3].SetFace(f0, 0);

                int[,] f1 = { {0, 1 }, { 0, 1 }, { 1, 1 }, { 0, 1 }, };
                kt[3].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[3].SetFace(f2, 2);

                int[,] f3 = { { 1, 0 }, { 1, 0 }, { 1, 1 }, { 1, 0 }, };
                kt[3].SetFace(f3, 3);
            }

            {
                int[,] f0 = { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, };
                kt[4].SetFace(f0, 0);

                int[,] f1 = { { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, };
                kt[4].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 1, 0 }, { 1, 0 }, { 1, 1 }, };
                kt[4].SetFace(f2, 2);

                int[,] f3 = { { 1, 0 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, };
                kt[4].SetFace(f3, 3);
            }

            {
                int[,] f0 = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[5].SetFace(f0, 0);

                int[,] f1 = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[5].SetFace(f1, 1);

                int[,] f2 = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[5].SetFace(f2, 2);

                int[,] f3 = { { 1, 1 }, { 1, 1 }, { 1, 1 }, { 1, 1 }, };
                kt[5].SetFace(f3, 3);
            }

        }
    }
}
