﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 組み木解法
{
    public partial class MainForm : Form
    {
        public static KumiTree[] Kt = new KumiTree[6];
        public static int NowParts = 0;
        public static int NowMen = 0;
        public static string ReadPartsFilename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Parts.csv";

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // メインフォームのタイトルをセットします
            SetMainFormTitle("", true);

            NowMen = 0;
            NowParts = 0;

            Kt[0] = new KumiTree();
            Kt[1] = new KumiTree();
            Kt[2] = new KumiTree();
            Kt[3] = new KumiTree();
            Kt[4] = new KumiTree();
            Kt[5] = new KumiTree();

            KumiType.SetType1(Kt);

            DispMen();
        }

        /// <summary>
        /// メインフォームのタイトルをセットします
        /// </summary>
        /// <param name="exportfilename"></param>
        /// <param name="buildpartFlg"></param>
        private void SetMainFormTitle(string exportfilename, bool buildpartFlg)
        {
            //自分自身のバージョン情報を取得する
            FileVersionInfo ver = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

            //タイトルの表示
            if (buildpartFlg == true)
            {
                this.Text = string.Concat("組み木解法", " Ver ", ver.FileMajorPart, ".", ver.FileMinorPart.ToString(), ".", ver.FileBuildPart.ToString());
            }
            else
            {
                this.Text = string.Concat("組み木解法", " Ver ", ver.FileMajorPart, ".", ver.FileMinorPart.ToString());
            }

            if (exportfilename != "")
            {
                this.Text = string.Concat(this.Text, "-", exportfilename);
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            int[,] f = new int[4, 2];

            f[0, 0] = int.Parse(tbx00.Text);
            f[0, 1] = int.Parse(tbx01.Text);
            f[1, 0] = int.Parse(tbx10.Text);
            f[1, 1] = int.Parse(tbx11.Text);
            f[2, 0] = int.Parse(tbx20.Text);
            f[2, 1] = int.Parse(tbx21.Text);
            f[3, 0] = int.Parse(tbx30.Text);
            f[3, 1] = int.Parse(tbx31.Text);

            Kt[NowParts].SetFace(f, int.Parse(tbxMenNum.Text));
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (Kt[NowParts].CheckFace() == false)
            {
                Debug.WriteLine("エラーです");
                tbxMessage.Text = "エラーです";
            }
            else
            {
                tbxMessage.Text = "O Kです";
            }
        }

        private void btnMenUp_Click(object sender, EventArgs e)
        {
            NowMen++;
            if (NowMen > 3)
            {
                NowMen = 0;
            }

            //面データをセットします
            DispMen();
        }

        private void btnMenDown_Click(object sender, EventArgs e)
        {
            NowMen--;
            if (NowMen < 0)
            {
                NowMen = 3;
            }

            //面データをセットします
            DispMen();
        }

        /// <summary>
        /// 面データをセットします
        /// </summary>
        private void DispMen()
        {
            tbxMenNum.Text = NowMen.ToString();

            int[,] f = Kt[NowParts].GetFace(NowMen);

            tbx00.Text = f[0, 0].ToString();
            tbx01.Text = f[0, 1].ToString();
            tbx10.Text = f[1, 0].ToString();
            tbx11.Text = f[1, 1].ToString();
            tbx20.Text = f[2, 0].ToString();
            tbx21.Text = f[2, 1].ToString();
            tbx30.Text = f[3, 0].ToString();
            tbx31.Text = f[3, 1].ToString();
        }

        private void btnPartsUp_Click(object sender, EventArgs e)
        {
            NowParts++;
            if (NowParts > 5)
            {
                NowParts = 5;
            }

            tbxParts.Text = NowParts.ToString();

            DispMen();
        }

        private void btnPartsDown_Click(object sender, EventArgs e)
        {
            NowParts--;
            if (NowParts < 0)
            {
                NowParts = 0;
            }

            tbxParts.Text = NowParts.ToString();

            DispMen();
        }

        /// <summary>
        /// 計算開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalc_Click(object sender, EventArgs e)
        {
            CalcStart(0);

            DispMen();
        }

        /// <summary>
        /// 全パターンの解析
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlAnal_Click(object sender, EventArgs e)
        {
            CalcStart(1);

            DispMen();
        }

        /// <summary>
        /// 解析します
        /// </summary>
        private void CalcStart(int mode)
        {
            List<int> result = new List<int>();

            int[] k = new int[6];
            k[0] = 0;
            k[1] = 1;
            k[2] = 2;
            k[3] = 3;
            k[4] = 4;
            k[5] = 5;

            Debug.WriteLine("---");

            List<int[]> partsList = new List<int[]>();

            //パーツは6個あるが、1個は固定位置で大丈夫なので、順番を入れ替えるのは、0～4の5個でいける。
            partsList = SetParts(k, 5);
            int cnt = partsList.Count;

            for (int i = 0; i < partsList.Count; i++)
            {
                tbxCount.Text = cnt.ToString();
                cnt--;
                Application.DoEvents();

                if (chkPiece(partsList[i]) == true)
                {
                    tbxMessage.Text = "解けました";
                    //解けていたら、パーツ順を入れ替える
                    KumiTree[] kt = new KumiTree[6];
                    kt[0] = Kt[partsList[i][0]].Rotate();
                    kt[1] = Kt[partsList[i][1]].Rotate();
                    kt[2] = Kt[partsList[i][2]].Rotate();
                    kt[3] = Kt[partsList[i][3]].Rotate();
                    kt[4] = Kt[partsList[i][4]].Rotate();
                    kt[5] = Kt[partsList[i][5]].Rotate();

                    Kt[0] = kt[0].Rotate();
                    Kt[1] = kt[1].Rotate();
                    Kt[2] = kt[2].Rotate();
                    Kt[3] = kt[3].Rotate();
                    Kt[4] = kt[4].Rotate();
                    Kt[5] = kt[5].Rotate();

                    string orders = partsList[i][0].ToString()
                                 + partsList[i][1].ToString()
                                 + partsList[i][2].ToString()
                                 + partsList[i][3].ToString()
                                 + partsList[i][4].ToString()
                                 + partsList[i][5].ToString();

                    tbxMessage.Text = "解けました: "
                                        + partsList[i][0].ToString() + ", "
                                        + partsList[i][1].ToString() + ", "
                                        + partsList[i][2].ToString() + ", "
                                        + partsList[i][3].ToString() + ", "
                                        + partsList[i][4].ToString() + ", "
                                        + partsList[i][5].ToString();

                    if (mode == 0)
                    {
                        return;
                    }
                    else
                    {
                        //解けたらそのデータを保存する
                        string fpath = Path.GetDirectoryName(ReadPartsFilename)+"\\";
                        string fname = Path.GetFileNameWithoutExtension(ReadPartsFilename);
                        DateTime dt = DateTime.Now;
                        //fname +=  dt.Year.ToString("0000") + dt.Month.ToString("00") + dt.Day.ToString("00") + dt.Hour.ToString("00") + dt.Minute.ToString("00") + dt.Second.ToString("00");
                        fname = fname + "_" + orders + ".csv";
                        SaveParts(fpath + fname);
                    }                    
                }
                Application.DoEvents();
            }

            if (mode == 0)
            {
                tbxMessage.Text = "解けませんでした";
            }
            else
            {
                tbxMessage.Text = "解析が終了しました";
            }


            //swp(k, 0, 0);
            //if (chkPiece(k) == true)
            //{
            //    tbxMessage.Text = "解けました";
            //    return;
            //}

            ////パーツは6個あるが、1個は固定位置で大丈夫なので、順番を入れ替えるのは、0～4の5個でいける。
            //if (recu(k, 5) == false)
            //{
            //    //解けていたら、パーツ順を入れ替える
            //    KumiTree[] kt = new KumiTree[6];
            //    kt[0] = Kt[k[0]].Rotate();
            //    kt[1] = Kt[k[1]].Rotate();
            //    kt[2] = Kt[k[2]].Rotate();
            //    kt[3] = Kt[k[3]].Rotate();
            //    kt[4] = Kt[k[4]].Rotate();
            //    kt[5] = Kt[k[5]].Rotate();

            //    Kt[0] = kt[0].Rotate();
            //    Kt[1] = kt[1].Rotate();
            //    Kt[2] = kt[2].Rotate();
            //    Kt[3] = kt[3].Rotate();
            //    Kt[4] = kt[4].Rotate();
            //    Kt[5] = kt[5].Rotate();
            //    return;
            //}
            //else
            //{
            //    tbxMessage.Text = "解けません";
            //}
        }

        /// <summary>
        /// dat配列の数値を順列の分だけ発生させます
        /// </summary>
        /// <param name="dat"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private List<int[]> SetParts(int[] dat, int n)
        {
            List<int[]> list = new List<int[]>();

            int[] k = (int[])dat.Clone();
            list.Add(k);

            SetPartsRecu(list, dat, n);

            return list;
        }

        /// <summary>
        /// 順列を求める再帰呼び出ルーチンです
        /// </summary>
        /// <param name="list"></param>
        /// <param name="dat"></param>
        /// <param name="n"></param>
        private void SetPartsRecu(List<int[]> list, int[] dat, int n)
        {
            if (n < 2) { return; }

            for (int i = 0; i < n; i++)
            {
                if (i > 0)
                {
                    swp(dat, 0, n - 1);
                    int[] k = (int[])dat.Clone();
                    list.Add(k);
                }
                SetPartsRecu(list, dat, n - 1);
            }
        }

        /// <summary>
        /// 衝突をチェックします
        /// </summary>
        /// <param name="k"></param>
        private bool chkPiece(int[] k)
        {
            KumiTree[] ktbak = new KumiTree[6];

            for (int i = 0; i < k.Length; i++)
            {
                Debug.Write(k[i].ToString() + ", ");
            }
            Debug.WriteLine("");

            for (int i = 0; i < 6; i++)
            {
                ktbak[i] = Kt[i].Rotate();
            }

            for (int a0 = 0; a0 < 2; a0++)
            {
                Kt[k[0]] = Kt[k[0]].Rotate();               //先に縦回転しておきます
                for (int m0 = 0; m0 < 4; m0++)
                {
                    Kt[k[0]] = Kt[k[0]].SiftFaceNum(m0);    //この面を0面にします
                    for (int a1 = 0; a1 < 2; a1++)
                    {
                        Kt[k[1]] = Kt[k[1]].Rotate();               //先に縦回転しておきます
                        for (int m1 = 0; m1 < 4; m1++)
                        {
                            Kt[k[1]] = Kt[k[1]].SiftFaceNum(m1);    //この面を0面にします
                            for (int a2 = 0; a2 < 2; a2++)
                            {
                                Kt[k[2]] = Kt[k[2]].Rotate();               //先に縦回転しておきます
                                for (int m2 = 0; m2 < 4; m2++)
                                {
                                    Kt[k[2]] = Kt[k[2]].SiftFaceNum(m1);    //この面を0面にします
                                    for (int a3 = 0; a3 < 2; a3++)
                                    {
                                        Kt[k[3]] = Kt[k[3]].Rotate();               //先に縦回転しておきます
                                        for (int m3 = 0; m3 < 4; m3++)
                                        {
                                            Kt[k[3]] = Kt[k[3]].SiftFaceNum(m1);    //この面を0面にします
                                            for (int a4 = 0; a4 < 2; a4++)
                                            {
                                                Kt[k[4]] = Kt[k[4]].Rotate();               //先に縦回転しておきます
                                                for (int m4 = 0; m4 < 4; m4++)
                                                {
                                                    Kt[k[4]] = Kt[k[4]].SiftFaceNum(m1);    //この面を0面にします
                                                    for (int a5 = 0; a5 < 2; a5++)
                                                    {
                                                        Kt[k[5]] = Kt[k[5]].Rotate();               //先に縦回転しておきます
                                                        for (int m5 = 0; m5 < 4; m5++)
                                                        {
                                                            Kt[k[5]] = Kt[k[5]].SiftFaceNum(m1);    //この面を0面にします

                                                            if (chkPieces(k) == true)
                                                            {
                                                                return true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < 6; i++)
            {
                Kt[i] = ktbak[i].Rotate();
            }

            return false;
        }

        private bool chkPieces(int[] k)
        {
            int p0 = k[0];
            int p1 = k[1];
            int p2 = k[2];
            int p3 = k[3];
            int p4 = k[4];
            int p5 = k[5];


            if (
                //P0-P2
                (Kt[p0].GetFace(0)[0, 0] != Kt[p2].GetFace(1)[2, 1] || Kt[p0].GetFace(0)[0, 0] == 0)
             && (Kt[p0].GetFace(0)[0, 1] != Kt[p2].GetFace(1)[1, 1] || Kt[p0].GetFace(0)[0, 1] == 0)
             && (Kt[p0].GetFace(0)[1, 0] != Kt[p2].GetFace(1)[2, 0] || Kt[p0].GetFace(0)[1, 0] == 0)
             && (Kt[p0].GetFace(0)[1, 1] != Kt[p2].GetFace(1)[1, 0] || Kt[p0].GetFace(0)[1, 1] == 0)
                //P0-P3
             && (Kt[p0].GetFace(0)[2, 0] != Kt[p3].GetFace(3)[2, 1] || Kt[p0].GetFace(0)[2, 0] == 0)
             && (Kt[p0].GetFace(0)[2, 1] != Kt[p3].GetFace(3)[1, 1] || Kt[p0].GetFace(0)[2, 1] == 0)
             && (Kt[p0].GetFace(0)[3, 0] != Kt[p3].GetFace(3)[2, 0] || Kt[p0].GetFace(0)[3, 0] == 0)
             && (Kt[p0].GetFace(0)[3, 1] != Kt[p3].GetFace(3)[1, 0] || Kt[p0].GetFace(0)[3, 1] == 0)
                //P0-P4
             && (Kt[p0].GetFace(3)[1, 0] != Kt[p4].GetFace(0)[0, 0] || Kt[p0].GetFace(3)[1, 0] == 0)
             && (Kt[p0].GetFace(3)[1, 1] != Kt[p4].GetFace(0)[1, 0] || Kt[p0].GetFace(3)[1, 1] == 0)
             && (Kt[p0].GetFace(3)[2, 0] != Kt[p4].GetFace(0)[0, 1] || Kt[p0].GetFace(3)[2, 0] == 0)
             && (Kt[p0].GetFace(3)[2, 1] != Kt[p4].GetFace(0)[1, 1] || Kt[p0].GetFace(3)[2, 1] == 0)
                //P0-P5
             && (Kt[p0].GetFace(1)[1, 0] != Kt[p5].GetFace(0)[1, 1] || Kt[p0].GetFace(1)[1, 0] == 0)
             && (Kt[p0].GetFace(1)[1, 1] != Kt[p5].GetFace(0)[0, 1] || Kt[p0].GetFace(1)[1, 1] == 0)
             && (Kt[p0].GetFace(1)[2, 0] != Kt[p5].GetFace(0)[1, 0] || Kt[p0].GetFace(1)[2, 0] == 0)
             && (Kt[p0].GetFace(1)[2, 1] != Kt[p5].GetFace(0)[0, 0] || Kt[p0].GetFace(1)[2, 1] == 0)

             //P1-P2
             && (Kt[p1].GetFace(0)[0, 0] != Kt[p2].GetFace(3)[1, 0] || Kt[p1].GetFace(0)[0, 0] == 0)
             && (Kt[p1].GetFace(0)[0, 1] != Kt[p2].GetFace(3)[2, 0] || Kt[p1].GetFace(0)[0, 1] == 0)
             && (Kt[p1].GetFace(0)[1, 0] != Kt[p2].GetFace(3)[1, 1] || Kt[p1].GetFace(0)[1, 0] == 0)
             && (Kt[p1].GetFace(0)[1, 1] != Kt[p2].GetFace(3)[2, 1] || Kt[p1].GetFace(0)[1, 1] == 0)
                //P1-P3
             && (Kt[p1].GetFace(0)[2, 0] != Kt[p3].GetFace(1)[1, 0] || Kt[p1].GetFace(0)[2, 0] == 0)
             && (Kt[p1].GetFace(0)[2, 1] != Kt[p3].GetFace(1)[2, 0] || Kt[p1].GetFace(0)[2, 1] == 0)
             && (Kt[p1].GetFace(0)[3, 0] != Kt[p3].GetFace(1)[1, 1] || Kt[p1].GetFace(0)[3, 0] == 0)
             && (Kt[p1].GetFace(0)[3, 1] != Kt[p3].GetFace(1)[2, 1] || Kt[p1].GetFace(0)[3, 1] == 0)
                //P1-P4
             && (Kt[p1].GetFace(1)[1, 0] != Kt[p4].GetFace(0)[2, 0] || Kt[p1].GetFace(1)[1, 0] == 0)
             && (Kt[p1].GetFace(1)[1, 1] != Kt[p4].GetFace(0)[3, 0] || Kt[p1].GetFace(1)[1, 1] == 0)
             && (Kt[p1].GetFace(1)[2, 0] != Kt[p4].GetFace(0)[2, 1] || Kt[p1].GetFace(1)[2, 0] == 0)
             && (Kt[p1].GetFace(1)[2, 1] != Kt[p4].GetFace(0)[3, 1] || Kt[p1].GetFace(1)[2, 1] == 0)
                //P1-P5
             && (Kt[p1].GetFace(3)[1, 0] != Kt[p5].GetFace(0)[3, 1] || Kt[p1].GetFace(3)[1, 0] == 0)
             && (Kt[p1].GetFace(3)[1, 1] != Kt[p5].GetFace(0)[2, 1] || Kt[p1].GetFace(3)[1, 1] == 0)
             && (Kt[p1].GetFace(3)[2, 0] != Kt[p5].GetFace(0)[3, 0] || Kt[p1].GetFace(3)[2, 0] == 0)
             && (Kt[p1].GetFace(3)[2, 1] != Kt[p5].GetFace(0)[2, 0] || Kt[p1].GetFace(3)[2, 1] == 0)


             //P2-P4
             && (Kt[p2].GetFace(0)[2, 0] != Kt[p4].GetFace(3)[2, 1] || Kt[p2].GetFace(0)[2, 0] == 0)
             && (Kt[p2].GetFace(0)[2, 1] != Kt[p4].GetFace(3)[1, 1] || Kt[p2].GetFace(0)[2, 1] == 0)
             && (Kt[p2].GetFace(0)[3, 0] != Kt[p4].GetFace(3)[2, 0] || Kt[p2].GetFace(0)[3, 0] == 0)
             && (Kt[p2].GetFace(0)[3, 1] != Kt[p4].GetFace(3)[1, 0] || Kt[p2].GetFace(0)[3, 1] == 0)
                //P2-P5
             && (Kt[p2].GetFace(0)[0, 0] != Kt[p5].GetFace(1)[2, 1] || Kt[p2].GetFace(0)[0, 0] == 0)
             && (Kt[p2].GetFace(0)[0, 1] != Kt[p5].GetFace(1)[1, 1] || Kt[p2].GetFace(0)[0, 1] == 0)
             && (Kt[p2].GetFace(0)[1, 0] != Kt[p5].GetFace(1)[2, 0] || Kt[p2].GetFace(0)[1, 0] == 0)
             && (Kt[p2].GetFace(0)[1, 1] != Kt[p5].GetFace(1)[1, 0] || Kt[p2].GetFace(0)[1, 1] == 0)

             //P3-P4
             && (Kt[p3].GetFace(0)[2, 0] != Kt[p4].GetFace(1)[2, 0] || Kt[p3].GetFace(0)[2, 0] == 0)
             && (Kt[p3].GetFace(0)[2, 1] != Kt[p4].GetFace(1)[1, 0] || Kt[p3].GetFace(0)[2, 1] == 0)
             && (Kt[p3].GetFace(0)[3, 0] != Kt[p4].GetFace(1)[2, 1] || Kt[p3].GetFace(0)[3, 0] == 0)
             && (Kt[p3].GetFace(0)[3, 1] != Kt[p4].GetFace(1)[1, 1] || Kt[p3].GetFace(0)[3, 1] == 0)
                //P3-P5
             && (Kt[p3].GetFace(0)[0, 0] != Kt[p5].GetFace(3)[1, 0] || Kt[p3].GetFace(0)[0, 0] == 0)
             && (Kt[p3].GetFace(0)[0, 1] != Kt[p5].GetFace(3)[2, 0] || Kt[p3].GetFace(0)[0, 1] == 0)
             && (Kt[p3].GetFace(0)[1, 0] != Kt[p5].GetFace(3)[1, 1] || Kt[p3].GetFace(0)[1, 0] == 0)
             && (Kt[p3].GetFace(0)[1, 1] != Kt[p5].GetFace(3)[2, 1] || Kt[p3].GetFace(0)[1, 1] == 0)
                )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// nこのデータを並べ替えます
        /// </summary>
        /// <param name="dat"></param>
        /// <param name="n"></param>
        private bool recu(int[] dat, int n)
        {
            if (n < 2) { return true; }

            for (int i = 0; i < n; i++)
            {
                if (i > 0)
                {
                    swp(dat, 0, n - 1);
                    if (chkPiece(dat) == true)
                    {
                        tbxMessage.Text = "解けました";
                        return false;
                    }
                }
                if (recu(dat, n - 1) == false)
                {
                    return false;
                }
            }
            return true;
        }


        private void swp(int[] dat, int i, int j)
        {
            int a = dat[i];
            dat[i] = dat[j];
            dat[j] = a;
        }

        /// <summary>
        /// パーツを縦に180度回します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRot_Click(object sender, EventArgs e)
        {
            KumiTree kt = Kt[NowParts].Rotate();

            if (kt != null)
            {
                Kt[NowParts] = kt;
            }
            DispMen();
        }

        /// <summary>
        /// 面番号と位置をシフトします
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenShift_Click(object sender, EventArgs e)
        {
            KumiTree kt = Kt[NowParts].SiftFaceNum(NowMen);

            if (kt != null)
            {
                Kt[NowParts] = kt;
            }
            DispMen();
        }

        /// <summary>
        /// パーツデータを保存します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Title = "組み木データ保存";                                            // タイトルバーの文字列

            sfd.InitialDirectory = Directory.GetCurrentDirectory();
            //sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);    //ダイアログの開く場所

            sfd.Filter = "CSV(*.csv)|*.csv|all files(*.*)|*.*";                     //「ファイルの種類」を指定

            sfd.FileName = "";

            if (sfd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            string fname = sfd.FileName;

            //保存します
            SaveParts(fname);

            sfd.Dispose();
        }

        /// <summary>
        /// 保存します
        /// </summary>
        /// <param name="fname"></param>
        private void SaveParts(string fname)
        {
            using (StreamWriter sw = new StreamWriter(fname, false, Encoding.UTF8))
            {
                string strdat = "";
                int[,] dat;
                for (int i = 0; i < 6; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        dat = Kt[i].GetFace(j);
                        for (int y = 0; y < 4; y++)
                        {
                            for (int x = 0; x < 2; x++)
                            {
                                strdat += dat[y, x].ToString() + ",";
                            }
                        }
                        strdat += "\r\n";
                    }
                }

                sw.Write(strdat);
            }

            tbxMessage.Text = Path.GetFileName(fname) + "に保存しました";
        }

        /// <summary>
        /// パーツデータを読み込みます
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRead_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            ofd.FileName = "";

            //はじめに表示されるフォルダを指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            ofd.InitialDirectory = @"";

            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            ofd.Filter = "csvファイル(*.csv)|*.csv|すべてのファイル(*.*)|*.*";

            //[ファイルの種類]ではじめに「*.exr」が選択されているようにする
            ofd.FilterIndex = 0;

            //タイトルを設定する
            ofd.Title = "組み木データの読み込み";

            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;

            //存在しないファイルの名前が指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckFileExists = true;

            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            //複数のファイルを選択できるようにする
            ofd.Multiselect = false;

            string filename = "";
            //ダイアログを表示する
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            //OKボタンがクリックされたとき
            ReadPartsFilename = ofd.FileName;

            string strdat = "";
            using (StreamReader sr = new StreamReader(ReadPartsFilename, Encoding.UTF8))
            {
                strdat = sr.ReadToEnd();
            }
            tbxMessage.Text = Path.GetFileName(ReadPartsFilename) + "を読み込みました";
            ofd.Dispose();

            string[] lines = strdat.Replace("\r", "").Split('\n');
            string[] dat;
            int[,] f = new int[4, 2];

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    dat = lines[i * 4 + j].Split(',');


                    for (int y = 0; y < 4; y++)
                    {
                        for (int x = 0; x < 2; x++)
                        {
                            f[y, x] = int.Parse(dat[y * 2 + x]);
                        }
                    }

                    Kt[i].SetFace(f, j);
                }
            }

            DispMen();
        }
    }
}

